import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DcOrderComponent } from './components/dc-order/dc-order.component';
import { OrderComponent } from './components/order/order.component';
import { ProfileComponent } from './components/profile/profile.component';
import { DashboardComponent } from './dashboard.component';

const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    children: [
      { path: '', redirectTo: 'customer', pathMatch: 'full' },
      {
        path: 'customer',
        loadChildren: () =>
          import('src/app/modules/customer/customer.module').then(
            (m) => m.CustomerModule
          ),
      },
      {
        path: 'card',
        loadChildren: () =>
          import('src/app/modules/card/card.module').then((m) => m.CardModule),
      },
      {
        path: 'price',
        loadChildren: () =>
          import('src/app/modules/price/price.module').then(
            (m) => m.PriceModule
          ),
      },
      {
        path: 'report',
        loadChildren: () =>
          import('src/app/modules/report/report.module').then(
            (m) => m.ReportModule
          ),
      },
      {
        path: 'transaction',
        loadChildren: () =>
          import('src/app/modules/transaction/transaction.module').then(
            (m) => m.TransactionModule
          ),
      },
      { path: 'order', component: OrderComponent },
      { path: 'dcorder', component: DcOrderComponent },
      { path: 'profile', component: ProfileComponent },
      { path: '**', redirectTo: 'customer', pathMatch: 'full' },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DashboardRoutingModule {}
