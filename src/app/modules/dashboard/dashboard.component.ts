import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { TranslateService } from '@ngx-translate/core';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {
  openSidebar: boolean = false;
  isObk: boolean = true;

  constructor(
    private authService: AuthService,
    public translate: TranslateService
  ) {}

  ngOnInit(): void {
    this.translate.addLangs(['en', 'ar']);
    this.translate.setDefaultLang('en');
    const browserLang = this.translate.getBrowserLang();
    this.translate.use(browserLang.match(/en|ar/) ? browserLang : 'en');
    if (localStorage.getItem('type') === 'telecom') this.isObk = false;
  }

  logout(): void {
    this.authService.logout();
  }

  translator(lang: string) {
    this.translate.use(lang);
    if (lang === 'ar') document.body.classList.add('dir_rtl');
    else document.body.classList.remove('dir_rtl');
  }
}
