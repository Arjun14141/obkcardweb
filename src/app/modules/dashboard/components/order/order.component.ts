import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { Category, Subcategory, Vender } from 'src/app/modules/card/_model';
import { CardService } from 'src/app/services/card.service';
import { OrderService } from 'src/app/services/order.service';
import { SnackBarService } from 'src/app/services/snack-bar.service';
import { HttpResponse } from 'src/app/shared/HTTPResponce.model';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss'],
})
export class OrderComponent implements OnInit, OnDestroy {
  paginator: any = { page: 1, pageSize: 9, total: 100 };
  private subscriptions: Subscription[] = [];
  isLoading: boolean = false;
  orderList: any[] = [];
  venderList: Vender[] = [];
  categoryList: Category[] = [];
  subCategoryList: Subcategory[] = [];
  filterGroup: FormGroup;
  searchGroup: FormGroup;
  searchText: string = '';
  currentVenderId: string = '';
  currentCategoryId: string = '';
  currentSubCategoryId: string = '';
  startRangeDate: Date;
  endRangeDate: Date;
  isDateOpen: boolean = false;

  constructor(
    private fb: FormBuilder,
    private orderService: OrderService,
    private cardService: CardService,
    private snackBarService: SnackBarService
  ) {}

  ngOnInit(): void {
    this.filterForm();
    this.searchForm();
    this.getOrderList(this.paginator.page, this.paginator.pageSize);
    this.getVenderList();
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((sb) => sb.unsubscribe());
  }

  getVenderList(): void {
    this.isLoading = true;
    const getVenderListSubscr = this.cardService.venderList().subscribe(
      (data: HttpResponse) => {
        if (data.status === 200) {
          this.venderList = data.data?.results;
          this.isLoading = false;
        }
      },
      (err: any) => {
        this.snackBarService.open({
          message: err.error.message,
          action: 'Error',
        });
        this.isLoading = false;
      }
    );
    this.subscriptions.push(getVenderListSubscr);
  }

  getCategoryList(venderId: string): void {
    if (!venderId) return;
    this.isLoading = true;
    const getCategoryListSubscr = this.cardService
      .categoryList(venderId)
      .subscribe(
        (data: HttpResponse) => {
          if (data.status === 200) this.categoryList = data.data?.results;
          this.isLoading = false;
        },
        (err: any) => {
          this.snackBarService.open({
            message: err.error.message,
            action: 'Error',
          });
          this.isLoading = false;
        }
      );
    this.subscriptions.push(getCategoryListSubscr);
  }

  getSubCategoryList(categoryId: string): void {
    if (!categoryId) return;
    this.isLoading = true;
    const getSubCategoryListSubscr = this.cardService
      .subCategoryList(categoryId)
      .subscribe(
        (data: HttpResponse) => {
          if (data.status === 200) this.subCategoryList = data.data?.results;
          this.isLoading = false;
        },
        (err: any) => {
          this.snackBarService.open({
            message: err.error.message,
            action: 'Error',
          });
          this.isLoading = false;
        }
      );
    this.subscriptions.push(getSubCategoryListSubscr);
  }

  getOrderList(page: number, pageSize: number): void {
    this.isLoading = true;
    const sb = this.orderService
      .orderList(
        page,
        pageSize,
        this.searchText,
        this.startRangeDate,
        this.endRangeDate,
        this.currentVenderId,
        this.currentCategoryId,
        this.currentSubCategoryId
      )
      .subscribe(
        (res: HttpResponse) => {
          if (res.status === 200) {
            this.orderList = res?.data?.results;
            this.paginator.total = res?.data?.totalResults;
            this.isLoading = false;
          }
        },
        (err: any) => {
          this.snackBarService.open({
            message: err.error.message,
            action: 'Error',
          });
          this.isLoading = false;
        }
      );
    this.subscriptions.push(sb);
  }
  //pagination
  paginate(paginator: any) {
    this.getOrderList(paginator.page, paginator.pageSize);
  }
  // search
  searchForm() {
    this.searchGroup = this.fb.group({ searchTerm: [''] });
    const searchEvent = this.searchGroup.controls.searchTerm.valueChanges
      .pipe(debounceTime(150), distinctUntilChanged())
      .subscribe((val) => this.search(val));
    this.subscriptions.push(searchEvent);
  }

  search(searchTerm: string) {
    this.searchText = searchTerm;
    this.getOrderList(1, this.paginator.pageSize);
  }

  //date filter
  filterForm() {
    this.filterGroup = this.fb.group({
      start: [''],
      end: [''],
      vendor: [''],
      category: [''],
      subCategory: [''],
    });
    this.subscriptions.push(
      this.filterGroup.controls.start.valueChanges.subscribe(() => {
        const startDate = this.filterGroup.get('start').value;
        if (!startDate) return;
        this.startRangeDate = startDate;
        this.getOrderList(1, this.paginator.pageSize);
      })
    );
    this.subscriptions.push(
      this.filterGroup.controls.end.valueChanges.subscribe(() => {
        const endDate = this.filterGroup.get('end').value;
        if (!endDate) return;
        this.endRangeDate = endDate;
        this.getOrderList(1, this.paginator.pageSize);
      })
    );
    const vendorEvent = this.filterGroup.controls.vendor.valueChanges.subscribe(
      (val) => {
        this.currentVenderId = val;
        this.categoryList = [];
        this.subCategoryList = [];
        this.currentCategoryId = '';
        this.currentSubCategoryId = '';
        this.filterGroup.get('category').setValue('');
        this.filterGroup.get('subCategory').setValue('');
        this.getCategoryList(val);
        this.getOrderList(1, this.paginator.pageSize);
      }
    );
    this.subscriptions.push(vendorEvent);
    const categoryEvent =
      this.filterGroup.controls.category.valueChanges.subscribe((val) => {
        this.currentCategoryId = val;
        this.subCategoryList = [];
        this.currentSubCategoryId = '';
        this.filterGroup.get('subCategory').setValue('');
        this.getSubCategoryList(val);
        this.getOrderList(1, this.paginator.pageSize);
      });
    this.subscriptions.push(categoryEvent);
    const subCategoryEvent =
      this.filterGroup.controls.subCategory.valueChanges.subscribe((val) => {
        this.currentSubCategoryId = val;
        this.getOrderList(1, this.paginator.pageSize);
      });
    this.subscriptions.push(subCategoryEvent);
  }

  clearDate(): void {
    this.endRangeDate = null;
    this.startRangeDate = null;
    this.filterGroup.get('start').setValue('');
    this.filterGroup.get('end').setValue('');
    this.getOrderList(1, this.paginator.pageSize);
  }
}
