import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { Customer } from 'src/app/modules/customer/_model';
import { CustomerService } from 'src/app/services/customer.service';
import { SnackBarService } from 'src/app/services/snack-bar.service';
import { HttpResponse } from 'src/app/shared/HTTPResponce.model';
import { EncryptService } from 'src/app/services/encrypt.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
})
export class ProfileComponent implements OnInit, OnDestroy {
  private subscribe: Subscription[] = [];
  isLoading: boolean = false;
  customerForm!: FormGroup;
  currentUser: Customer;
  hasError: boolean = false;
  isPassword: boolean = false;
  isConfirmPassword: boolean = false;

  constructor(
    private fb: FormBuilder,
    private snackBarService: SnackBarService,
    private customerService: CustomerService,
    private encryptService: EncryptService
  ) {}

  ngOnInit(): void {
    this.customerForm = this.fb.group({
      name: ['', Validators.required],
      address: ['', Validators.required],
      email: ['', Validators.required],
      password: ['', Validators.required],
      confirmPassword: ['', Validators.required],
      // currencyId: ['61a0805a0e90bc38d0a15a45', Validators.required],
    });
    this.currentUser = JSON.parse(localStorage.getItem('user'));
    if (this.currentUser) {
      this.customerForm.patchValue(this.currentUser);
      this.customerForm
        .get('password')
        .setValue(this.encryptService.decrypt(this.currentUser.password));
      this.customerForm
        .get('confirmPassword')
        .setValue(this.encryptService.decrypt(this.currentUser.password));
    }
  }

  ngOnDestroy(): void {
    this.subscribe.forEach((sb) => sb.unsubscribe());
  }

  submit(): void {
    if (
      this.customerForm.value.password ===
      this.customerForm.value.confirmPassword
    ) {
      this.isLoading = true;
      this.hasError = false;
      const payload = {
        name: this.customerForm.value.name,
        address: this.customerForm.value.address,
        email: this.customerForm.value.email,
        password: this.customerForm.value.password,
      };
      const updateAdminSubscr = this.customerService
        .updateProfile(payload)
        .subscribe(
          (customer: HttpResponse) => {
            if (customer.status === 200) {
              customer.data.password = this.encryptService.crypt(
                customer?.data?.password
              );
              localStorage.setItem('user', JSON.stringify(customer.data));
              this.snackBarService.open({
                message: customer.message,
                action: 'Success',
              });
              this.isLoading = false;
            }
          },
          (err: any) => {
            this.snackBarService.open({
              message: err.error.message,
              action: 'Error',
            });
            this.isLoading = false;
          }
        );
      this.subscribe.push(updateAdminSubscr);
    } else this.hasError = true;
  }

  // helpers for View
  isControlValid(controlName: string) {
    const control = this.customerForm.controls[controlName];
    return control.valid && (control.dirty || control.touched);
  }

  isControlInvalid(controlName: string) {
    const control = this.customerForm.controls[controlName];
    return control.invalid && (control.dirty || control.touched);
  }

  controlHasError(validation: string, controlName: string) {
    const control = this.customerForm.controls[controlName];
    return control.hasError(validation) && (control.dirty || control.touched);
  }

  isControlTouched(controlName: string) {
    const control = this.customerForm.controls[controlName];
    return control.dirty || control.touched;
  }
}
