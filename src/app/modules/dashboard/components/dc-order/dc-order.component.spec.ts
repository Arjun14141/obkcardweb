import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DcOrderComponent } from './dc-order.component';

describe('DcOrderComponent', () => {
  let component: DcOrderComponent;
  let fixture: ComponentFixture<DcOrderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DcOrderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DcOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
