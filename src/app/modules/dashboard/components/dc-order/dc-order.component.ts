import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { OrderService } from 'src/app/services/order.service';
import { SnackBarService } from 'src/app/services/snack-bar.service';
import { HttpResponse } from 'src/app/shared/HTTPResponce.model';

@Component({
  selector: 'app-dc-order',
  templateUrl: './dc-order.component.html',
  styleUrls: ['./dc-order.component.scss'],
})
export class DcOrderComponent implements OnInit, OnDestroy {
  paginator: any = { page: 1, pageSize: 9, total: 100 };
  private subscriptions: Subscription[] = [];
  isLoading: boolean = false;
  dcOrderList: any[] = [];
  isDateOpen: boolean = false;
  searchText: string = '';
  startDate: Date;
  endDate: Date = new Date();

  constructor(
    private snackBarService: SnackBarService,
    private orderService: OrderService
  ) {}

  ngOnInit(): void {
    this.getOrderList(this.paginator.page, this.paginator.pageSize);
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((sb) => sb.unsubscribe());
  }

  getOrderList(page: number, pageSize: number): void {
    this.isLoading = true;
    const sb = this.orderService
      .DCOrderList(page, pageSize, this.searchText, '', '')
      .subscribe(
        (res: HttpResponse) => {
          if (res.status === 200) {
            this.dcOrderList = res?.data?.results;
            this.paginator.total = res?.data?.totalResults;
            this.isLoading = false;
          }
        },
        (err: any) => {
          this.snackBarService.open({ message: err.error.message, action: 'Error' });
          this.isLoading = false;
        }
      );
    this.subscriptions.push(sb);
  }

  completeOrder(id: any): void {
    if (!id) return;
    this.isLoading = true;
    const sb = this.orderService.completeOrder(id).subscribe(
      (res: HttpResponse) => {
        if (res.status === 200) {
          this.getOrderList(this.paginator.page, this.paginator.pageSize);
          this.snackBarService.open({
            message: res.message,
            action: 'Success',
          });
          this.isLoading = false;
        }
      },
      (err: any) => {
        this.snackBarService.open({ message: err.error.message, action: 'Error' });
        this.isLoading = false;
      }
    );
    this.subscriptions.push(sb);
  }

  //pagination
  paginate(paginator: any) {
    this.getOrderList(paginator.page, paginator.pageSize);
  }
}
