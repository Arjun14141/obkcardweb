import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { LandingComponent } from 'src/app/modules/home/components/landing/landing.component';
import { PrivacyComponent } from 'src/app/modules/home/components/privacy/privacy.component';
import { TermComponent } from 'src/app/modules/home/components/term/term.component';
import { MaterialsModule } from 'src/app/shared/material.module';
import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';

@NgModule({
  declarations: [
    HomeComponent,
    LandingComponent,
    TermComponent,
    PrivacyComponent,
  ],
  imports: [
    CommonModule,
    HomeRoutingModule,
    MaterialsModule,
    SlickCarouselModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
  ],
})
export class HomeModule {}
