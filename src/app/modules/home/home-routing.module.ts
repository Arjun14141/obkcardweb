import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LandingComponent } from 'src/app/modules/home/components/landing/landing.component';
import { PrivacyComponent } from 'src/app/modules/home/components/privacy/privacy.component';
import { TermComponent } from 'src/app/modules/home/components/term/term.component';
import { HomeComponent } from './home.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    children: [
      { path: '', redirectTo: 'landing', pathMatch: 'full' },
      { path: 'landing', component: LandingComponent },
      { path: 'term', component: TermComponent },
      { path: 'privacy', component: PrivacyComponent },
      { path: '**', redirectTo: 'landing', pathMatch: 'full' },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomeRoutingModule {}
