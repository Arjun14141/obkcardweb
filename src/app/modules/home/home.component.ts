import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { first } from 'rxjs/operators';
import { AuthService } from 'src/app/services/auth.service';
import { SnackBarService } from 'src/app/services/snack-bar.service';
import { HttpResponse } from 'src/app/shared/HTTPResponce.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit, OnDestroy {
  private subscribe: Subscription[] = [];
  isLoading: boolean = false;
  loginForm!: FormGroup;
  isMobile: boolean = false;
  isSignUp: boolean = false;

  @ViewChild('radioTelecom') radioTelecom: any;

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private authService: AuthService,
    private snackBarService: SnackBarService
  ) {}

  ngOnInit(): void {
    this.initForm();
  }

  ngOnDestroy(): void {
    this.subscribe.forEach((sb) => sb.unsubscribe());
  }

  contacNav(): void {
    this.router.navigate(['home/landing'], {
      queryParams: { isContact: true },
    });
  }

  openSignUp(): void {
    this.isSignUp = true;
    this.isMobile = false;
    document.body.classList.add('blockoverflow');
  }

  closeSignUp(): void {
    this.isSignUp = false;
    document.body.classList.remove('blockoverflow');
  }

  initForm(): void {
    this.loginForm = this.fb.group({
      username: [
        '',
        Validators.compose([
          Validators.required,
          Validators.email,
          Validators.minLength(3),
          Validators.maxLength(320),
        ]),
      ],
      password: [
        '',
        Validators.compose([
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(100),
        ]),
      ],
    });
  }

  submit(): void {
    this.isLoading = true;
    let type = 'obk';
    if (this.radioTelecom.nativeElement.checked) type = 'telecom';
    localStorage.setItem('type', type);
    const loginSubscr = this.authService
      .login(this.loginForm.value)
      .pipe(first())
      .subscribe(
        (user: HttpResponse) => {
          console.log(user, 'ts');
          if (user?.data && user?.data?.role === 'admin') {
            if (user?.data?.isDeleted) {
              this.snackBarService.open({
                message: 'User is blocked',
                action: 'Error',
              });
              return;
            }
            document.body?.classList.remove('blockoverflow');
            this.snackBarService.open({
              message: 'User login Successfully',
              action: 'Success',
            });
            this.router.navigate(['dash/customer']);
          } else {
            this.snackBarService.open({
              message: 'incorrect id or password ',
              action: 'Error',
            });
          }
          this.isLoading = false;
        },
        (err) => {
          console.log(err);
          this.isLoading = true;
        }
      );
    this.subscribe.push(loginSubscr);
  }
}
