import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss'],
})
export class LandingComponent implements OnInit {
  isUser: boolean = false;
  isUser2: boolean = true;
  slideConfig: any = {
    arrows: true,
    prevArrow:
      "<button type='button' class='slick-prev'><i class='fas fa-chevron-left'></i></button>",
    nextArrow:
      "<button type='button' class='slick-next'><i class='fas fa-chevron-right'></i></button>",
    dots: false,
  };

  constructor(private route: ActivatedRoute) {}

  ngOnInit(): void {
    this.route.queryParams.subscribe((params) => {
      if (params?.isContact)
        document.getElementById('contactUS')?.scrollIntoView({
          behavior: 'smooth',
          block: 'start',
          inline: 'nearest',
        });
    });
  }
}
