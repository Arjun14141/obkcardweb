export interface Price {
  id: string; // '61a0aa391de9ea44d0db6521';
  amount: number;
  currencyId: Id;
  customerId: Id; // {name: "C1", id: "61aeef9c04efcb351f2b8b5c"}
  venderId: Id; // {name: "C1", id: "61aeef9c04efcb351f2b8b5c"}
  categoryId: Id; // "618e09bf5ce5403284fa73a1",
  subCategoryId: Id; // "618e0d587201070ab07f6681",
  isDeleted: boolean; // false,
  updatedBy: string; // null;
  createdBy: string; // '61a0805a0e90bc38d0a15a45';
  createdAt: string; // '2021-12-08T09:58:55.616Z';
  deletedAt: string; // null;
  deletedBy: string; // null;
  updatedAt: string; // '2021-12-08T09:58:55.616Z';
}

interface Id {
  name: string;
  id: string;
}
