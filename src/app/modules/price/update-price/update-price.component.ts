import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { CardService } from 'src/app/services/card.service';
import { CustomerService } from 'src/app/services/customer.service';
import { PriceService } from 'src/app/services/price.service';
import { SnackBarService } from 'src/app/services/snack-bar.service';
import { HttpResponse } from 'src/app/shared/HTTPResponce.model';
import { Category, Subcategory, Vender } from '../../card/_model';
import { Customer } from '../../customer/_model';
import { Price } from './../_model';

@Component({
  selector: 'app-update-price',
  templateUrl: './update-price.component.html',
  styleUrls: ['./update-price.component.scss'],
})
export class UpdatePriceComponent implements OnInit, OnDestroy {
  private subscribe: Subscription[] = [];
  isLoading: boolean = false;
  customerList: Customer[] = [];
  venderList: Vender[] = [];
  categoryList: Category[] = [];
  subCategoryList: Subcategory[] = [];
  priceForm!: FormGroup;
  priceData: Price;
  currentUser: Customer;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private customerService: CustomerService,
    private priceService: PriceService,
    private cardService: CardService,
    private snackBarService: SnackBarService
  ) {}

  ngOnInit(): void {
    this.priceForm = this.fb.group({
      amount: ['', Validators.required],
      customerId: ['', Validators.required],
      venderId: ['', Validators.required],
      categoryId: ['', Validators.required],
      subCategoryId: ['', Validators.required],
    });
    this.currentUser = JSON.parse(localStorage.getItem('user'));
    this.activatedRoute.params.subscribe((params) => {
      if (params && params.id) this.getPriceData(params.id);
    });
    setTimeout(() => {
      this.getVenderList();
      this.getCustomerList();
    }, 10);
  }

  ngOnDestroy(): void {
    this.subscribe.forEach((sb) => sb.unsubscribe());
  }

  getPriceData(priceId: string): void {
    this.isLoading = true;
    const getPriceDataSubscr = this.priceService.getPrice(priceId).subscribe(
      (data: HttpResponse) => {
        if (data.status === 200) {
          this.priceData = data.data;
          this.priceForm.get('amount').setValue(this.priceData?.amount);
          this.isLoading = false;
        }
      },
      (err: any) => {
        this.snackBarService.open({
          message: err.error.message,
          action: 'Error',
        });
        this.isLoading = false;
      }
    );
    this.subscribe.push(getPriceDataSubscr);
  }

  getCustomerList(): void {
    this.isLoading = true;
    const getCustomerListSubscr = this.customerService
      .customerList(1, 9999,'')
      .subscribe(
        (data: HttpResponse) => {
          if (data.status === 200) {
            this.customerList = data.data?.results;
            this.priceForm
              .get('customerId')
              .setValue(this.priceData?.customerId?.id);
            this.isLoading = false;
          }
        },
        (err: any) => {
          this.snackBarService.open({
            message: err.error.message,
            action: 'Error',
          });
          this.isLoading = false;
        }
      );
    this.subscribe.push(getCustomerListSubscr);
  }

  getVenderList(): void {
    this.isLoading = true;
    const getVenderListSubscr = this.cardService.venderList().subscribe(
      (data: HttpResponse) => {
        if (data.status === 200) {
          this.venderList = data.data?.results;
          this.priceForm.get('venderId').setValue(this.priceData?.venderId?.id);
          this.isLoading = false;
        }
      },
      (err: any) => {
        this.snackBarService.open({
          message: err.error.message,
          action: 'Error',
        });
        this.isLoading = false;
      }
    );
    this.subscribe.push(getVenderListSubscr);
  }

  getCategoryList(venderId: string): void {
    if (!venderId) return;
    this.isLoading = true;
    const getCategoryListSubscr = this.cardService
      .categoryList(venderId)
      .subscribe(
        (data: HttpResponse) => {
          if (data.status === 200) {
            this.categoryList = data.data?.results;
            this.priceForm
              .get('categoryId')
              .setValue(this.priceData?.categoryId?.id);
            this.isLoading = false;
          }
        },
        (err: any) => {
          this.snackBarService.open({
            message: err.error.message,
            action: 'Error',
          });
          this.isLoading = false;
        }
      );
    this.subscribe.push(getCategoryListSubscr);
  }

  getSubCategoryList(categoryId: string): void {
    if (!categoryId) return;
    this.isLoading = true;
    const getSubCategoryListSubscr = this.cardService
      .subCategoryList(categoryId)
      .subscribe(
        (data: HttpResponse) => {
          if (data.status === 200) {
            this.subCategoryList = data.data?.results;
            this.priceForm
              .get('subCategoryId')
              .setValue(this.priceData?.subCategoryId?.id);
            this.isLoading = false;
          }
        },
        (err: any) => {
          this.snackBarService.open({
            message: err.error.message,
            action: 'Error',
          });
          this.isLoading = false;
        }
      );
    this.subscribe.push(getSubCategoryListSubscr);
  }

  submit(): void {
    this.isLoading = true;
    const payload = {
      currencyId: this.currentUser?.currencyId?.id,
      ...this.priceForm.value,
    };
    const createPriceSubscr = this.priceService
      .updatePrice(payload, this.priceData.id)
      .subscribe(
        (customer: HttpResponse) => {
          if (customer.status === 200) {
            this.router.navigate(['dash/price']);
            this.snackBarService.open({
              message: customer.message,
              action: 'Success',
            });
            this.isLoading = false;
          }
        },
        (err: any) => {
          this.snackBarService.open({
            message: err.error.message,
            action: 'Error',
          });
          this.isLoading = false;
        }
      );
    this.subscribe.push(createPriceSubscr);
  }

  // helpers for View
  isControlValid(controlName: string) {
    const control = this.priceForm.controls[controlName];
    return control.valid && (control.dirty || control.touched);
  }

  isControlInvalid(controlName: string) {
    const control = this.priceForm.controls[controlName];
    return control.invalid && (control.dirty || control.touched);
  }

  controlHasError(validation: string, controlName: string) {
    const control = this.priceForm.controls[controlName];
    return control.hasError(validation) && (control.dirty || control.touched);
  }

  isControlTouched(controlName: string) {
    const control = this.priceForm.controls[controlName];
    return control.dirty || control.touched;
  }
}
