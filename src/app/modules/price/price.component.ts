import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { PriceService } from 'src/app/services/price.service';
import { SnackBarService } from 'src/app/services/snack-bar.service';
import { HttpResponse } from 'src/app/shared/HTTPResponce.model';
import { Price } from './_model';

@Component({
  selector: 'app-price',
  templateUrl: './price.component.html',
  styleUrls: ['./price.component.scss'],
})
export class PriceComponent implements OnInit, OnDestroy {
  paginator: any = { page: 1, pageSize: 9, total: 100 };
  private subscriptions: Subscription[] = [];
  isLoading: boolean = false;
  priceList: Price[] = [];
  currentPrice!: Price;
  isDelete: boolean = false;

  constructor(
    private router: Router,
    private priceService: PriceService,
    private snackBarService: SnackBarService
  ) {}

  ngOnInit(): void {
    this.getPriceList(this.paginator.page, this.paginator.pageSize);
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((sb) => sb.unsubscribe());
  }

  getPriceList(page: number, pageSize: number): void {
    this.isLoading = true;
    const sb = this.priceService.priceList(page, pageSize).subscribe(
      (res: HttpResponse) => {
        this.priceList = res?.data?.results;
        this.paginator.total = res?.data?.totalResults;
        this.isLoading = false;
      },
      (err: any) => {
        this.snackBarService.open({ message: err.error.message, action: 'Error' });
        this.isLoading = false;
      }
    );
    this.subscriptions.push(sb);
  }
  getCurrentCustomer(price: Price) {
    this.isDelete = true;
    document.body.classList.add('blockoverflow');
    this.currentPrice = price;
  }
  closeDelete() {
    this.isDelete = false;
    document.body.classList.remove('blockoverflow');
  }
  updatePriceNavigate(price: { id: string }): void {
    this.router.navigate(['dash/price/update', { id: price.id }]);
  }
  //pagination
  paginate(paginator: any) {
    this.getPriceList(paginator.page, paginator.pageSize);
  }
  deletePrice(): void {
    if (!this.currentPrice.id) return;
    this.isLoading = true;
    const sb = this.priceService.deletePrice(this.currentPrice.id).subscribe(
      (data: HttpResponse) => {
        if (data.status === 200) {
          this.getPriceList(this.paginator.page, this.paginator.pageSize);
          this.snackBarService.open({
            message: data.message,
            action: 'Success',
          });
          this.isLoading = false;
          this.isDelete = false;
          document.body.classList.remove('blockoverflow');
        }
      },
      (err: any) => {
        this.snackBarService.open({ message: err.error.message, action: 'Error' });
        this.isLoading = false;
        this.isDelete = false;
        document.body.classList.remove('blockoverflow');
      }
    );
    this.subscriptions.push(sb);
  }
}
