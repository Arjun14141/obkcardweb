import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EditPriceComponent } from './edit-price/edit-price.component';
import { PriceComponent } from './price.component';
import { UpdatePriceComponent } from './update-price/update-price.component';

const routes: Routes = [
  { path: '', component: PriceComponent },
  { path: 'edit', component: EditPriceComponent },
  { path: 'update', component: UpdatePriceComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PriceRoutingModule {}
