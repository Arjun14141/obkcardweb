import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { CardService } from 'src/app/services/card.service';
import { CustomerService } from 'src/app/services/customer.service';
import { PriceService } from 'src/app/services/price.service';
import { SnackBarService } from 'src/app/services/snack-bar.service';
import { HttpResponse } from 'src/app/shared/HTTPResponce.model';
import { Category, Subcategory, Vender } from '../../card/_model';
import { Customer } from '../../customer/_model';

@Component({
  selector: 'app-edit-price',
  templateUrl: './edit-price.component.html',
  styleUrls: ['./edit-price.component.scss'],
})
export class EditPriceComponent implements OnInit, OnDestroy {
  private subscribe: Subscription[] = [];
  isLoading: boolean = false;
  customerList: Customer[] = [];
  venderList: Vender[] = [];
  categoryList: Category[] = [];
  subCategoryList: Subcategory[] = [];
  priceForm!: FormGroup;
  currentUser: Customer;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private customerService: CustomerService,
    private priceService: PriceService,
    private cardService: CardService,
    private snackBarService: SnackBarService
  ) {}

  ngOnInit(): void {
    this.priceForm = this.fb.group({
      amount: ['', Validators.required],
      customerId: ['', Validators.required],
      venderId: ['', Validators.required],
      categoryId: ['', Validators.required],
      subCategoryId: ['', Validators.required],
    });
    this.currentUser = JSON.parse(localStorage.getItem('user'));
    this.getVenderList();
    this.getCustomerList();
  }

  ngOnDestroy(): void {
    this.subscribe.forEach((sb) => sb.unsubscribe());
  }

  getCustomerList(): void {
    this.isLoading = true;
    const getCustomerListSubscr = this.customerService
      .customerList(1, 9999,'')
      .subscribe(
        (data: HttpResponse) => {
          if (data.status === 200) {
            this.customerList = data.data?.results;
            this.isLoading = false;
          }
        },
        (err: any) => {
          this.snackBarService.open({
            message: err.error.message,
            action: 'Error',
          });
          this.isLoading = false;
        }
      );
    this.subscribe.push(getCustomerListSubscr);
  }

  getVenderList(): void {
    this.isLoading = true;
    const getVenderListSubscr = this.cardService.venderList().subscribe(
      (data: HttpResponse) => {
        if (data.status === 200) {
          this.venderList = data.data?.results;
          this.isLoading = false;
        }
      },
      (err: any) => {
        this.snackBarService.open({
          message: err.error.message,
          action: 'Error',
        });
        this.isLoading = false;
      }
    );
    this.subscribe.push(getVenderListSubscr);
  }

  getCategoryList(venderId: string): void {
    if (!venderId) return;
    this.isLoading = true;
    const getCategoryListSubscr = this.cardService
      .categoryList(venderId)
      .subscribe(
        (data: HttpResponse) => {
          if (data.status === 200) {
            this.categoryList = data.data?.results;
            this.subCategoryList = [];
            this.isLoading = false;
          }
        },
        (err: any) => {
          this.snackBarService.open({
            message: err.error.message,
            action: 'Error',
          });
          this.isLoading = false;
        }
      );
    this.subscribe.push(getCategoryListSubscr);
  }

  getSubCategoryList(categoryId: string): void {
    if (!categoryId) return;
    this.isLoading = true;
    const getSubCategoryListSubscr = this.cardService
      .subCategoryList(categoryId)
      .subscribe(
        (data: HttpResponse) => {
          if (data.status === 200) this.subCategoryList = data.data?.results;
          this.isLoading = false;
        },
        (err: any) => {
          this.snackBarService.open({
            message: err.error.message,
            action: 'Error',
          });
          this.isLoading = false;
        }
      );
    this.subscribe.push(getSubCategoryListSubscr);
  }

  submit(): void {
    this.isLoading = true;
    const payload = {
      currencyId: this.currentUser?.currencyId?.id,
      ...this.priceForm.value,
    };
    const createPriceSubscr = this.priceService.createPrice(payload).subscribe(
      (customer: HttpResponse) => {
        if (customer.status === 200) {
          this.router.navigate(['dash/price']);
          this.snackBarService.open({
            message: customer.message,
            action: 'Success',
          });
          this.isLoading = false;
        }
      },
      (err: any) => {
        this.snackBarService.open({
          message: err.error.message,
          action: 'Error',
        });
        this.isLoading = false;
      }
    );
    this.subscribe.push(createPriceSubscr);
  }

  reset(): void {
    this.priceForm.reset();
    this.isLoading = false;
  }
  // helpers for View
  isControlValid(controlName: string) {
    const control = this.priceForm.controls[controlName];
    return control.valid && (control.dirty || control.touched);
  }

  isControlInvalid(controlName: string) {
    const control = this.priceForm.controls[controlName];
    return control.invalid && (control.dirty || control.touched);
  }

  controlHasError(validation: string, controlName: string) {
    const control = this.priceForm.controls[controlName];
    return control.hasError(validation) && (control.dirty || control.touched);
  }

  isControlTouched(controlName: string) {
    const control = this.priceForm.controls[controlName];
    return control.dirty || control.touched;
  }
}
