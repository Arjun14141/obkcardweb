import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { MaterialsModule } from 'src/app/shared/material.module';
import { EditPriceComponent } from './edit-price/edit-price.component';
import { PriceRoutingModule } from './price-routing.module';
import { PriceComponent } from './price.component';
import { UpdatePriceComponent } from './update-price/update-price.component';

@NgModule({
  declarations: [PriceComponent, EditPriceComponent, UpdatePriceComponent],
  imports: [
    CommonModule,
    PriceRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialsModule,
    TranslateModule
  ],
})
export class PriceModule {}
