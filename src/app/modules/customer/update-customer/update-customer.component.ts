import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { CustomerService } from 'src/app/services/customer.service';
import { SnackBarService } from 'src/app/services/snack-bar.service';
import { HttpResponse } from 'src/app/shared/HTTPResponce.model';
import { Customer } from '../_model';
import { RefillModalComponent } from './refill-modal/refill-modal.component';
import { UpdateBalanceModalComponent } from './update-balance-modal/update-balance-modal.component';

@Component({
  selector: 'app-update-customer',
  templateUrl: './update-customer.component.html',
  styleUrls: ['./update-customer.component.scss'],
})
export class UpdateCustomerComponent implements OnInit, OnDestroy {
  private subscribe: Subscription[] = [];
  isLoading: boolean = false;
  customerForm!: FormGroup;
  customerData!: Customer;
  currentUser: Customer;
  hasError: boolean = false;
  isPassword: boolean = true;
  isConfirmPassword: boolean = true;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private customerService: CustomerService,
    private snackBarService: SnackBarService,
    public dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.customerForm = this.fb.group({
      name: ['', Validators.required],
      address: ['', Validators.required],
      contact: ['', Validators.required],
      balance: ['', Validators.required],
      password: ['', Validators.required],
      confirmPassword: ['', Validators.required],
    });
    this.currentUser = JSON.parse(localStorage.getItem('user'));
    this.activatedRoute.params.subscribe((param) => {
      if (param && param.id) this.getData(param.id);
    });
  }

  ngOnDestroy(): void {
    this.subscribe.forEach((sb) => sb.unsubscribe());
  }

  getData(id: string): void {
    this.isLoading = true;
    const getCustomerSubscr = this.customerService.getCustomer(id).subscribe(
      (customer: HttpResponse) => {
        if (customer.status === 200 && customer.data) {
          this.customerData = customer.data;
          this.customerForm.patchValue(this.customerData);
          this.customerForm
            .get('confirmPassword')
            ?.setValue(this.customerData.password);
          this.isLoading = false;
        }
      },
      (err: any) => {
        this.snackBarService.open({ message: err.error.message, action: 'Error' });
        this.isLoading = false;
      }
    );
    this.subscribe.push(getCustomerSubscr);
  }

  submit(): void {
    if (
      this.customerForm.value.password ===
      this.customerForm.value.confirmPassword
    ) {
      this.isLoading = true;
      this.hasError = false;
      const payload = {
        name: this.customerForm.value.name,
        address: this.customerForm.value.address,
        contact: this.customerForm.value.contact,
        email: this.customerData.email,
        balance: this.customerForm.value.balance,
        password: this.customerForm.value.password,
        currencyId: this.currentUser?.currencyId?.id,
      };
      const createCustomerSubscr = this.customerService
        .updateCustomer(payload, this.customerData.id)
        .subscribe(
          (customer: HttpResponse) => {
            if (customer.status === 200) {
              this.router.navigate(['dash/customer']);
              this.snackBarService.open({
                message: customer.message,
                action: 'Success',
              });
              this.isLoading = false;
            }
          },
          (err: any) => {
            this.snackBarService.open({
              message: err.error.message,
              action: 'Error',
            });
            this.isLoading = false;
          }
        );
      this.subscribe.push(createCustomerSubscr);
    } else this.hasError = true;
  }

  unblock(): void {
    this.isLoading = true;
    const sb = this.customerService
      .unblockCustomer(this.customerData.id)
      .subscribe(
        (customer: HttpResponse) => {
          if (customer.status === 200) {
            this.customerData = customer.data;
            this.snackBarService.open({
              message: customer.message,
              action: 'Success',
            });
            this.isLoading = false;
          }
        },
        (err: any) => {
          this.snackBarService.open({ message: err.error.message, action: 'Error' });
          this.isLoading = false;
        }
      );
    this.subscribe.push(sb);
  }

  openRefillDialog(): void {
    const dialogRef = this.dialog.open(RefillModalComponent, {
      data: { refillBalance: 0 },
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.isLoading = true;
        const sb = this.customerService
          .AddBalance(this.customerData.id, +result)
          .subscribe(
            (customer: HttpResponse) => {
              if (customer.status === 200) {
                this.customerForm
                  .get('balance')
                  ?.setValue(this.customerForm.value.balance + +result);
                this.snackBarService.open({
                  message: customer.message,
                  action: 'Success',
                });
                this.isLoading = false;
              }
            },
            (err: any) => {
              this.snackBarService.open({
                message: err.error.message,
                action: 'Error',
              });
              this.isLoading = true;
            }
          );
        this.subscribe.push(sb);
      }
    });
  }

  openUpdateDialog(): void {
    const dialogRef = this.dialog.open(UpdateBalanceModalComponent, {
      data: { updateBalance: 0 },
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.isLoading = true;
        const sb = this.customerService
          .updateBalance(this.customerData.id, +result)
          .subscribe(
            (customer: HttpResponse) => {
              if (customer.status === 200) {
                this.customerForm.get('balance')?.setValue(+result);
                this.snackBarService.open({
                  message: customer.message,
                  action: 'Success',
                });
                this.isLoading = false;
              }
            },
            (err: any) => {
              this.snackBarService.open({
                message: err.error.message,
                action: 'Error',
              });
              this.isLoading = true;
            }
          );
        this.subscribe.push(sb);
      }
    });
  }

  // helpers for View
  isControlValid(controlName: string) {
    const control = this.customerForm.controls[controlName];
    return control.valid && (control.dirty || control.touched);
  }

  isControlInvalid(controlName: string) {
    const control = this.customerForm.controls[controlName];
    return control.invalid && (control.dirty || control.touched);
  }

  controlHasError(validation: string, controlName: string) {
    const control = this.customerForm.controls[controlName];
    return control.hasError(validation) && (control.dirty || control.touched);
  }

  isControlTouched(controlName: string) {
    const control = this.customerForm.controls[controlName];
    return control.dirty || control.touched;
  }
}
