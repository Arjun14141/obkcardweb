import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

export interface DialogData {
  refillBalance: number;
}

@Component({
  selector: 'app-refill-modal',
  templateUrl: './refill-modal.component.html',
  styleUrls: ['./refill-modal.component.scss'],
})
export class RefillModalComponent implements OnInit {
  constructor(
    public dialogRef: MatDialogRef<RefillModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {}

  ngOnInit(): void {}

  onNoClick(): void {
    this.dialogRef.close();
  }
}
