import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RefillModalComponent } from './refill-modal.component';

describe('RefillModalComponent', () => {
  let component: RefillModalComponent;
  let fixture: ComponentFixture<RefillModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RefillModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RefillModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
