import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

export interface DialogData {
  updateBalance: number;
}

@Component({
  selector: 'app-update-balance-modal',
  templateUrl: './update-balance-modal.component.html',
  styleUrls: ['./update-balance-modal.component.scss'],
})
export class UpdateBalanceModalComponent implements OnInit {
  constructor(
    public dialogRef: MatDialogRef<UpdateBalanceModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {}

  ngOnInit(): void {}

  onNoClick(): void {
    this.dialogRef.close();
  }
}
