import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { CustomerService } from 'src/app/services/customer.service';
import { SnackBarService } from 'src/app/services/snack-bar.service';
import { HttpResponse } from 'src/app/shared/HTTPResponce.model';
import { Customer } from '../_model';

@Component({
  selector: 'app-edit-customer',
  templateUrl: './edit-customer.component.html',
  styleUrls: ['./edit-customer.component.scss'],
})
export class EditCustomerComponent implements OnInit, OnDestroy {
  private subscribe: Subscription[] = [];
  isLoading: boolean = false;
  customerForm!: FormGroup;
  currentUser: Customer;
  hasError: boolean = false;
  isPassword: boolean = false;
  isConfirmPassword: boolean = false;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private customerService: CustomerService,
    private snackBarService: SnackBarService
  ) {}

  ngOnInit(): void {
    this.customerForm = this.fb.group({
      name: ['', Validators.required],
      address: ['', Validators.required],
      contact: ['', Validators.required],
      email: ['', Validators.required],
      balance: ['', Validators.required],
      password: ['', Validators.required],
      confirmPassword: ['', Validators.required],
      // currencyId: ['61a0805a0e90bc38d0a15a45', Validators.required],
    });
    this.currentUser = JSON.parse(localStorage.getItem('user'));
  }

  ngOnDestroy(): void {
    this.subscribe.forEach((sb) => sb.unsubscribe());
  }

  submit(): void {
    if (
      this.customerForm.value.password ===
      this.customerForm.value.confirmPassword
      ) {
        this.isLoading = true;
        this.hasError = false;
      const payload = {
        name: this.customerForm.value.name,
        address: this.customerForm.value.address,
        contact: this.customerForm.value.contact,
        email: this.customerForm.value.email,
        balance: this.customerForm.value.balance,
        password: this.customerForm.value.password,
        currencyId: this.currentUser?.currencyId?.id,
      };
      const createCustomerSubscr = this.customerService
        .createCustomer(payload)
        .subscribe(
          (customer: HttpResponse) => {
            if (customer.status === 200) {
              this.router.navigate(['dash/customer']);
              this.snackBarService.open({
                message: customer.message,
                action: 'Success',
              });
              this.isLoading = false;
            }
          },
          (err: any) => {
            this.snackBarService.open({
              message: err.error.message,
              action: 'Error',
            });
            this.isLoading = false;
          }
        );
      this.subscribe.push(createCustomerSubscr);
    } else this.hasError = true;
  }

  // helpers for View
  isControlValid(controlName: string) {
    const control = this.customerForm.controls[controlName];
    return control.valid && (control.dirty || control.touched);
  }

  isControlInvalid(controlName: string) {
    const control = this.customerForm.controls[controlName];
    return control.invalid && (control.dirty || control.touched);
  }

  controlHasError(validation: string, controlName: string) {
    const control = this.customerForm.controls[controlName];
    return control.hasError(validation) && (control.dirty || control.touched);
  }

  isControlTouched(controlName: string) {
    const control = this.customerForm.controls[controlName];
    return control.dirty || control.touched;
  }
}
