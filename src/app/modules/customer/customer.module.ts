import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CustomerRoutingModule } from './customer-routing.module';
import { CustomerComponent } from './customer.component';
import { EditCustomerComponent } from './edit-customer/edit-customer.component';
import { UpdateCustomerComponent } from './update-customer/update-customer.component';
import { RefillModalComponent } from './update-customer/refill-modal/refill-modal.component';
import { UpdateBalanceModalComponent } from './update-customer/update-balance-modal/update-balance-modal.component';
import { MaterialsModule } from 'src/app/shared/material.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    CustomerComponent,
    EditCustomerComponent,
    UpdateCustomerComponent,
    RefillModalComponent,
    UpdateBalanceModalComponent,
  ],
  imports: [
    CommonModule,
    CustomerRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule,
    MaterialsModule,
  ],
})
export class CustomerModule {}
