import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { debounceTime, distinctUntilChanged, Subscription } from 'rxjs';
import { CustomerService } from 'src/app/services/customer.service';
import { SnackBarService } from 'src/app/services/snack-bar.service';
import { HttpResponse } from 'src/app/shared/HTTPResponce.model';
import { Customer } from './_model';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.scss'],
})
export class CustomerComponent implements OnInit, OnDestroy {
  paginator: any = { page: 1, pageSize: 9, total: 100 };
  private subscriptions: Subscription[] = [];
  searchGroup: FormGroup;
  searchText: string = '';
  isLoading: boolean = false;
  customerList: Customer[] = [];
  currentCustomer!: Customer;
  isEdit: boolean = false;
  isDelete: boolean = false;

  constructor(
    private fb: FormBuilder,
    private customerService: CustomerService,
    private router: Router,
    private snackBarService: SnackBarService
  ) {}

  ngOnInit(): void {
    this.searchForm();
    this.getCustomerList(this.paginator.page, this.paginator.pageSize);
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((sb) => sb.unsubscribe());
  }

  getCustomerList(page: number, pageSize: number): void {
    this.isLoading = true;
    const sb = this.customerService.customerList(page, pageSize, this.searchText).subscribe(
      (res: HttpResponse) => {
        this.customerList = res?.data?.results;
        this.paginator.total = res?.data?.totalResults;
        this.isLoading = false;
      },
      (err: any) => {
        this.snackBarService.open({ message: err.error.message, action: 'Error' });
        this.isLoading = false;
      }
    );
    this.subscriptions.push(sb);
  }

  updateCustomerNavigate(customer: { id: string }): void {
    this.router.navigate(['dash/customer/update', { id: customer.id }]);
  }

  getCurrentCustomer(customer: Customer) {
    this.isDelete = true;
    this.currentCustomer = customer;
    document.body.classList.add('blockoverflow');
  }

  closeDelete() {
    this.isDelete = false;
    document.body.classList.remove('blockoverflow');
  }

    // search
    searchForm() {
      this.searchGroup = this.fb.group({ searchTerm: [''] });
      const searchEvent = this.searchGroup.controls.searchTerm.valueChanges
        .pipe(debounceTime(150), distinctUntilChanged())
        .subscribe((val) => this.search(val));
      this.subscriptions.push(searchEvent);
    }

    search(searchTerm: string) {
      this.searchText = searchTerm;
      this.getCustomerList(1, this.paginator.pageSize);
    }

  //pagination
  paginate(paginator: any) {
    this.getCustomerList(paginator.page, paginator.pageSize);
  }
  deleteCustomer() {
    if (!this.currentCustomer.id) return;
    this.isLoading = true;
    const sb = this.customerService
      .deleteCustomer(this.currentCustomer.id)
      .subscribe(
        (data: HttpResponse) => {
          if (data.status === 200) {
            this.getCustomerList(this.paginator.page, this.paginator.pageSize);
            this.snackBarService.open({
              message: data.message,
              action: 'Success',
            });
            this.isLoading = false;
            this.isDelete = false;
            document.body.classList.remove('blockoverflow');
          }
        },
        (err: any) => {
          this.snackBarService.open({ message: err.error.message, action: 'Error' });
          this.isLoading = false;
          this.isDelete = false;
          document.body.classList.remove('blockoverflow');
        }
      );
    this.subscriptions.push(sb);
  }
}
