export interface Customer {
  id: string; // '61a0aa391de9ea44d0db6521';
  name: string; // 'Customer2';
  email: string; // 'c2@gmail.com';
  password: string; // 'Pass@12345';
  address: string; // "2",
  role: string; // "customer",
  contact: number; // 7894561230,
  balance: number; // 50,
  currencyId: Currency; // "618e4771cf87da1d5061c1c5",
  isDeleted: boolean; // false,
  deletedAt: Date; // null,
  deletedBy: string; // null;
  updatedAt: Date; // null;
  updatedBy: string; // null;
  createdBy: string; // '61a0805a0e90bc38d0a15a45';
  createdAt: string;
}

interface Currency {
  id: string;
  currency: string;
  name: string;
}
