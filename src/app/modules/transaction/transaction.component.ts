import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { CustomerService } from 'src/app/services/customer.service';
import { SnackBarService } from 'src/app/services/snack-bar.service';
import { HttpResponse } from 'src/app/shared/HTTPResponce.model';
import { Customer } from '../customer/_model';

@Component({
  selector: 'app-transaction',
  templateUrl: './transaction.component.html',
  styleUrls: ['./transaction.component.scss'],
})
export class TransactionComponent implements OnInit {
  paginator: any = { page: 1, pageSize: 9, total: 100 };
  private subscriptions: Subscription[] = [];
  isLoading: boolean = false;
  currentCustomerId: string = '';
  customerList: Customer[] = [];
  transactionList: any[] = [];
  currentUser: Customer;
  reportForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private customerService: CustomerService,
    private snackBarService: SnackBarService
  ) {}

  ngOnInit(): void {
    this.statusForm();
    this.currentUser = JSON.parse(localStorage.getItem('user'));
    this.getCustomerList();
  }

  getCustomerList(): void {
    this.isLoading = true;
    const getCustomerListSubscr = this.customerService
      .customerList(1, 9999, '')
      .subscribe(
        (data: HttpResponse) => {
          if (data.status === 200) {
            this.customerList = data.data?.results;
            this.isLoading = false;
          }
        },
        (err: any) => {
          this.snackBarService.open({
            message: err.error.message,
            action: 'Error',
          });
          this.isLoading = false;
        }
      );
    this.subscriptions.push(getCustomerListSubscr);
  }

  getTransactionList(page: number, pageSize: number, customerId: string): void {
    if (!customerId) return;
    this.isLoading = true;
    const ListSubscr = this.customerService
      .transactionList(page, pageSize, this.currentUser.id, customerId)
      .subscribe(
        (data: HttpResponse) => {
          if (data.status === 200) {
            this.transactionList = data.data?.results;
            this.paginator.total = data?.data?.totalResults;
            this.isLoading = false;
          }
        },
        (err: any) => {
          this.snackBarService.open({
            message: err.error.message,
            action: 'Error',
          });
          this.isLoading = false;
        }
      );
    this.subscriptions.push(ListSubscr);
  }
  paginate(paginator: any) {
    this.getTransactionList(
      paginator.page,
      paginator.pageSize,
      this.currentCustomerId
    );
  }

  statusForm() {
    this.reportForm = this.fb.group({ customer: [''] });
    const customerEvent =
      this.reportForm.controls.customer.valueChanges.subscribe((val) => {
        this.currentCustomerId = val;
        if (val) this.getTransactionList(1, 9, val);
        else this.transactionList = [];
      });
    this.subscriptions.push(customerEvent);
  }
}
