import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { CardService } from 'src/app/services/card.service';
import { SnackBarService } from 'src/app/services/snack-bar.service';
import { HttpResponse } from 'src/app/shared/HTTPResponce.model';
import { Customer } from '../customer/_model';
import { Card, Category, Subcategory, Vender } from './_model';
import * as XLSX from 'xlsx';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss'],
})
export class CardComponent implements OnInit, OnDestroy {
  paginator: any = { page: 1, pageSize: 9, total: 100 };
  isLoading: boolean = false;
  private subscriptions: Subscription[] = [];
  cardList: Card[] = [];
  venderList: Vender[] = [];
  categoryList: Category[] = [];
  subCategoryList: Subcategory[] = [];
  errorList: string[] = [];
  // searchGroup: FormGroup;
  statusGroup: FormGroup;
  currentUser: Customer;
  currentCard!: Card;
  file: File;
  searchText: string = '';
  currentVenderId: string = '';
  currentCategoryId: string = '';
  currentSubCategoryId: string = '';
  isDelete: boolean = false;
  isError: boolean = false;
  isStatusOpen: boolean = false;

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private cardService: CardService,
    private snackBarService: SnackBarService
  ) {}

  ngOnInit(): void {
    // this.searchForm();
    this.statusForm();
    this.currentUser = JSON.parse(localStorage.getItem('user'));
    if (this.currentUser?.id)
      this.getCardList(
        this.paginator.page,
        this.paginator.pageSize,
        this.currentUser?.id
      );
    this.getVenderList();
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((sb) => sb.unsubscribe());
  }

  getVenderList(): void {
    this.isLoading = true;
    const getVenderListSubscr = this.cardService.venderList().subscribe({
      next: (data: HttpResponse) => {
        if (data.status === 200) {
          this.venderList = data.data?.results;
          this.isLoading = false;
        }
      },
      error: (err: any) => {
        this.snackBarService.open({
          message: err.error.message,
          action: 'Error',
        });
        this.isLoading = false;
      },
    });
    this.subscriptions.push(getVenderListSubscr);
  }

  getCategoryList(venderId: string): void {
    if (!venderId) return;
    this.isLoading = true;
    const getCategoryListSubscr = this.cardService
      .categoryList(venderId)
      .subscribe({
        next: (data: HttpResponse) => {
          if (data.status === 200) this.categoryList = data.data?.results;
          this.isLoading = false;
        },
        error: (err: any) => {
          this.snackBarService.open({
            message: err.error.message,
            action: 'Error',
          });
          this.isLoading = false;
        },
      });
    this.subscriptions.push(getCategoryListSubscr);
  }

  getSubCategoryList(categoryId: string): void {
    if (!categoryId) return;
    this.isLoading = true;
    const getSubCategoryListSubscr = this.cardService
      .subCategoryList(categoryId)
      .subscribe({
        next: (data: HttpResponse) => {
          if (data.status === 200) this.subCategoryList = data.data?.results;
          this.isLoading = false;
        },
        error: (err: any) => {
          this.snackBarService.open({
            message: err.error.message,
            action: 'Error',
          });
          this.isLoading = false;
        },
      });
    this.subscriptions.push(getSubCategoryListSubscr);
  }

  getCardList(page: number, pageSize: number, adminId: string): void {
    this.isLoading = true;
    const sb = this.cardService
      .cardList(
        page,
        pageSize,
        adminId,
        this.searchText,
        this.currentVenderId,
        this.currentCategoryId,
        this.currentSubCategoryId
      )
      .subscribe({
        next: (res: HttpResponse) => {
          if (res.status === 200) {
            this.cardList = res?.data?.results;
            this.paginator.total = res?.data?.totalResults;
            this.isLoading = false;
          }
        },
        error: (err: any) => {
          this.snackBarService.open({
            message: err.error.message,
            action: 'Error',
          });
          this.isLoading = false;
        },
      });
    this.subscriptions.push(sb);
  }

  updateCardNavigate(card: { id: string }): void {
    this.router.navigate(['dash/card/update', { id: card.id }]);
  }

  statusForm(): void {
    this.statusGroup = this.fb.group({
      status: [''],
      vendor: [''],
      category: [''],
      subCategory: [''],
    });
    const statusEvent = this.statusGroup.controls.status.valueChanges.subscribe(
      (val) => {
        this.searchText = val;
        this.getCardList(1, this.paginator.pageSize, this.currentUser.id);
      }
    );
    this.subscriptions.push(statusEvent);
    const vendorEvent = this.statusGroup.controls.vendor.valueChanges.subscribe(
      (val) => {
        this.currentVenderId = val;
        if (val === '') {
          this.categoryList = [];
          this.subCategoryList = [];
          this.currentCategoryId = '';
          this.currentSubCategoryId = '';
          this.statusGroup.get('category').setValue('');
          this.statusGroup.get('subCategory').setValue('');
        } else this.getCategoryList(val);
        this.getCardList(1, this.paginator.pageSize, this.currentUser.id);
      }
    );
    this.subscriptions.push(vendorEvent);
    const categoryEvent =
      this.statusGroup.controls.category.valueChanges.subscribe((val) => {
        this.currentCategoryId = val;
        if (val === '') {
          this.subCategoryList = [];
          this.currentSubCategoryId = '';
          this.statusGroup.get('subCategory').setValue('');
        } else this.getSubCategoryList(val);
        this.getCardList(1, this.paginator.pageSize, this.currentUser.id);
      });
    this.subscriptions.push(categoryEvent);
    const subCategoryEvent =
      this.statusGroup.controls.subCategory.valueChanges.subscribe((val) => {
        this.currentSubCategoryId = val;
        this.getCardList(1, this.paginator.pageSize, this.currentUser.id);
      });
    this.subscriptions.push(subCategoryEvent);
  }

  getCurrentCard(card: Card): void {
    this.isDelete = true;
    document.body.classList.add('blockoverflow');
    this.currentCard = card;
  }

  closeDelete(): void {
    this.isDelete = false;
    document.body.classList.remove('blockoverflow');
  }
  //pagination
  paginate(paginator: any): void {
    this.getCardList(paginator.page, paginator.pageSize, this.currentUser.id);
  }

  deleteCard(): void {
    if (!this.currentCard.id) return;
    this.isLoading = true;
    const sb = this.cardService.deleteCard(this.currentCard.id).subscribe(
      (data: HttpResponse) => {
        if (data.status === 200) {
          this.getCardList(
            this.paginator.page,
            this.paginator.pageSize,
            this.currentUser.id
          );
          this.snackBarService.open({
            message: data.message,
            action: 'Success',
          });
          this.isLoading = false;
          this.isDelete = false;
          document.body.classList.remove('blockoverflow');
        }
      },
      (err: any) => {
        this.snackBarService.open({
          message: err.error.message,
          action: 'Error',
        });
        this.isLoading = false;
        this.isDelete = false;
        document.body.classList.remove('blockoverflow');
      }
    );
    this.subscriptions.push(sb);
  }

  importExcel(event: any): void {
    if (!event.target.files[0]) return;
    let arraylist = [];
    let fileReader = new FileReader();
    fileReader.readAsArrayBuffer(event.target.files[0]);
    fileReader.onload = (e) => {
      let arrayBuffer: any = fileReader.result;
      let data = new Uint8Array(arrayBuffer);
      let arr = new Array();
      for (let i = 0; i != data.length; ++i)
        arr[i] = String.fromCharCode(data[i]);
      let bstr = arr.join('');
      let workbook = XLSX.read(bstr, { type: 'binary' });
      let first_sheet_name = workbook.SheetNames[0];
      let worksheet = workbook.Sheets[first_sheet_name];
      arraylist = XLSX.utils.sheet_to_json(worksheet, { raw: true });
      console.log(arraylist);
      if (arraylist.length > 0) {
        this.isLoading = true;
        const importCardSubscr = this.cardService
          .importCardList(arraylist)
          .subscribe({
            next: (data: HttpResponse) => {
              if (data.status === 200) {
                this.snackBarService.open({
                  message: data.message,
                  action: 'Success',
                });
                this.getCardList(
                  this.paginator.page,
                  this.paginator.pageSize,
                  this.currentUser?.id
                );
                this.isLoading = false;
              }
            },
            error: (err: any) => {
              this.snackBarService.open({
                message: err.error.message,
                action: 'Error',
              });
              this.isLoading = false;
              if (err.error.data.error.length > 0) {
                this.errorList = err.error.data.error;
                this.isError = true;
              }
            },
          });
        this.subscriptions.push(importCardSubscr);
      }
    };
  }
}
