import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { CardService } from 'src/app/services/card.service';
import { SnackBarService } from 'src/app/services/snack-bar.service';
import { HttpResponse } from 'src/app/shared/HTTPResponce.model';
import { Category, Subcategory, Vender } from '../_model';

@Component({
  selector: 'app-edit-card',
  templateUrl: './edit-card.component.html',
  styleUrls: ['./edit-card.component.scss'],
})
export class EditCardComponent implements OnInit, OnDestroy {
  private subscribe: Subscription[] = [];
  isLoading: boolean = false;
  venderList: Vender[] = [];
  categoryList: Category[] = [];
  subCategoryList: Subcategory[] = [];
  cardForm!: FormGroup;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private cardService: CardService,
    private snackBarService: SnackBarService
  ) {}

  ngOnInit(): void {
    this.cardForm = this.fb.group({
      cardNumber: ['', Validators.required],
      serialNumber: ['', Validators.required],
      venderId: ['', Validators.required],
      categoryId: ['', Validators.required],
      subCategoryId: ['', Validators.required],
    });
    this.getVenderList();
  }

  ngOnDestroy(): void {
    this.subscribe.forEach((sb) => sb.unsubscribe());
  }

  getVenderList(): void {
    this.isLoading = true;
    const getVenderListSubscr = this.cardService.venderList().subscribe(
      (data: HttpResponse) => {
        if (data.status === 200) {
          this.venderList = data.data?.results;
          this.isLoading = false;
        }
      },
      (err: any) => {
        this.snackBarService.open({
          message: err.error.message,
          action: 'Error',
        });
        this.isLoading = false;
      }
    );
    this.subscribe.push(getVenderListSubscr);
  }

  getCategoryList(venderId: string): void {
    if (!venderId) return;
    this.isLoading = true;
    const getCategoryListSubscr = this.cardService
      .categoryList(venderId)
      .subscribe(
        (data: HttpResponse) => {
          if (data.status === 200) {
            this.categoryList = data.data?.results;
            this.subCategoryList = [];
            this.isLoading = false;
          }
        },
        (err: any) => {
          this.snackBarService.open({
            message: err.error.message,
            action: 'Error',
          });
          this.isLoading = false;
        }
      );
    this.subscribe.push(getCategoryListSubscr);
  }

  getSubCategoryList(categoryId: string): void {
    if (!categoryId) return;
    this.isLoading = true;
    const getSubCategoryListSubscr = this.cardService
      .subCategoryList(categoryId)
      .subscribe(
        (data: HttpResponse) => {
          if (data.status === 200) {
            this.subCategoryList = data.data?.results;
            this.isLoading = false;
          }
        },
        (err: any) => {
          this.snackBarService.open({
            message: err.error.message,
            action: 'Error',
          });
          this.isLoading = false;
        }
      );
    this.subscribe.push(getSubCategoryListSubscr);
  }

  submit(): void {
    this.isLoading = true;
    const payload = {
      cardNumber: this.cardForm.value.cardNumber + '',
      serialNumber: this.cardForm.value.serialNumber + '',
      venderId: this.cardForm.value.venderId,
      venderName: '',
      categoryId: this.cardForm.value.categoryId,
      categoryName: '',
      subCategoryId: this.cardForm.value.subCategoryId,
      subCategoryName: '',
    };
    this.venderList.forEach((vender) => {
      if (vender && vender.id === payload.venderId)
        payload.venderName = vender.name;
    });
    this.categoryList.forEach((category) => {
      if (category && category.id === payload.categoryId)
        payload.categoryName = category.name;
    });
    this.subCategoryList.forEach((subCategory) => {
      if (subCategory && subCategory.id === payload.subCategoryId)
        payload.subCategoryName = subCategory.name;
    });
    const createCardSubscr = this.cardService.createCard(payload).subscribe(
      (customer: HttpResponse) => {
        if (customer.status === 200) {
          this.router.navigate(['dash/card']);
          this.snackBarService.open({
            message: customer.message,
            action: 'Success',
          });
          this.isLoading = false;
        }
      },
      (err: any) => {
        this.snackBarService.open({
          message: err.error.message,
          action: 'Error',
        });
        this.isLoading = false;
      }
    );
    this.subscribe.push(createCardSubscr);
  }

  reset(): void {
    this.cardForm.reset();
    this.isLoading = false;
  }
  // helpers for View
  isControlValid(controlName: string) {
    const control = this.cardForm.controls[controlName];
    return control.valid && (control.dirty || control.touched);
  }

  isControlInvalid(controlName: string) {
    const control = this.cardForm.controls[controlName];
    return control.invalid && (control.dirty || control.touched);
  }

  controlHasError(validation: string, controlName: string) {
    const control = this.cardForm.controls[controlName];
    return control.hasError(validation) && (control.dirty || control.touched);
  }

  isControlTouched(controlName: string) {
    const control = this.cardForm.controls[controlName];
    return control.dirty || control.touched;
  }
}
