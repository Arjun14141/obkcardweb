export interface Card {
  id: string; // '61a0aa391de9ea44d0db6521';
  venderId: Populate; // {name: "C1", id: "61aeef9c04efcb351f2b8b5c"}
  venderName: string; // "V2",
  categoryId: string; // "618e09bf5ce5403284fa73a1",
  categoryName: string; // "C1",
  subCategoryId: string; // "618e0d587201070ab07f6681",
  subCategoryName: string; // "SC1",
  isAvailable: string; // "Available",
  cardNumber: string; // "987654321019",
  serialNumber: string; // "987654321012",
  isDeleted: boolean; // false,
  deletedAt: Date; // null,
  deletedBy: string; // null;
  updatedAt: Date; // null;
  updatedBy: string; // null;
  orderId: string;
  buyBy: Populate;
  buyAt: string;
  createdBy: string; // '61a0805a0e90bc38d0a15a45';
  createdAt: string;
}

export interface Populate {
  name: string;
  id: string;
}
export interface Vender {
  id: string; // '61a0aa391de9ea44d0db6521';
  imageURL: string; // "Joi.string().required()",
  isDirectCharge: boolean; // true,
  name: string; // "Vender1",
  isDeleted: boolean; // false,
  deletedAt: Date; // null,
  deletedBy: string; // null;
  updatedAt: Date; // null;
  updatedBy: string; // null;
  createdBy: string; // '61a0805a0e90bc38d0a15a45';
  createdAt: string;
}

export interface Category {
  id: string; // '61a0aa391de9ea44d0db6521';
  amount: number; // 10,
  currencyId: Object; //
  imageURL: string; // "v1",
  name: string; // "C1",
  venderId: Object; // "618df81ef92b641e10aa69f2",
  isDeleted: boolean; // false,
  deletedAt: Date; // null,
  deletedBy: string; // null;
  updatedAt: Date; // null;
  updatedBy: string; // null;
  createdBy: string; // '61a0805a0e90bc38d0a15a45';
  createdAt: string;
  subCategoryList: any[]
}

export interface Subcategory {
  id: string; // '61a0aa391de9ea44d0db6521';
  amount: number; // 10,
  currency: string; // "INR",
  imageURL: string; // "s1",
  name: string; // "S1",
  categoryId: Object; // "618e09bf5ce5403284fa73a1",
  isDeleted: boolean; // false,
  deletedAt: Date; // null,
  deletedBy: string; // null;
  updatedAt: Date; // null;
  updatedBy: string; // null;
  createdBy: string; // '61a0805a0e90bc38d0a15a45';
  createdAt: string;
}
