import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { MaterialsModule } from 'src/app/shared/material.module';
import { CardRoutingModule } from './card-routing.module';
import { CardComponent } from './card.component';
import { EditCardComponent } from './edit-card/edit-card.component';
import { UpdateCardComponent } from './update-card/update-card.component';

@NgModule({
  declarations: [CardComponent, EditCardComponent, UpdateCardComponent],
  imports: [
    CommonModule,
    CardRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialsModule,
    TranslateModule
  ],
})
export class CardModule {}
