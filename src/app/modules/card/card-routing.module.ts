import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CardComponent } from './card.component';
import { EditCardComponent } from './edit-card/edit-card.component';
import { UpdateCardComponent } from './update-card/update-card.component';

const routes: Routes = [
  { path: '', component: CardComponent },
  { path: 'edit', component: EditCardComponent },
  { path: 'update', component: UpdateCardComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CardRoutingModule {}
