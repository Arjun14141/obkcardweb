import { FormBuilder, FormGroup } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Category, Subcategory, Vender } from '../card/_model';
import { CardService } from 'src/app/services/card.service';
import { SnackBarService } from 'src/app/services/snack-bar.service';
import { HttpResponse } from 'src/app/shared/HTTPResponce.model';
import { Customer } from '../customer/_model';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.scss'],
})
export class ReportComponent implements OnInit {
  private subscriptions: Subscription[] = [];
  isLoading: boolean = false;
  venderList: Vender[] = [];
  categoryList: Category[] = [];
  subCategoryList: Subcategory[] = [];
  currentUser: Customer;
  searchText: string = '';
  currentVenderId: string = '';
  currentCategoryId: string = '';
  reportForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private cardService: CardService,
    private snackBarService: SnackBarService
  ) {}

  ngOnInit(): void {
    this.statusForm();
    this.currentUser = JSON.parse(localStorage.getItem('user'));
    this.getVenderList();
    this.getCategoryList('');
  }

  getVenderList(): void {
    this.isLoading = true;
    const getVenderListSubscr = this.cardService.venderList().subscribe(
      (data: HttpResponse) => {
        if (data.status === 200) {
          this.venderList = data.data?.results;
          this.isLoading = false;
        }
      },
      (err: any) => {
        this.snackBarService.open({
          message: err.error.message,
          action: 'Error',
        });
        this.isLoading = false;
      }
    );
    this.subscriptions.push(getVenderListSubscr);
  }

  getCategoryList(venderId: string): void {
    this.isLoading = true;
    const getCategoryListSubscr = this.cardService
      .categoryList(venderId)
      .subscribe(
        (data: HttpResponse) => {
          if (data.status === 200) {
            this.categoryList = data.data?.results;
          }
          this.isLoading = false;
        },
        (err: any) => {
          this.snackBarService.open({
            message: err.error.message,
            action: 'Error',
          });
          this.isLoading = false;
        }
      );
    this.subscriptions.push(getCategoryListSubscr);
  }

  getSubCategoryList(categoryId: string): void {
    if (!categoryId) return;
    this.isLoading = true;
    const getSubCategoryListSubscr = this.cardService
      .reportList(
        this.currentUser.id,
        this.searchText,
        this.currentVenderId,
        categoryId
      )
      .subscribe(
        (data: HttpResponse) => {
          if (data.status === 200) {
            this.isLoading = false;
            this.categoryList.forEach((category) => {
              if (category && category.id === categoryId) {
                category['subCategoryList'] = data.data.results;
              }
            });
          }
        },
        (err: any) => {
          this.snackBarService.open({
            message: err.error.message,
            action: 'Error',
          });
          this.isLoading = false;
        }
      );
    this.subscriptions.push(getSubCategoryListSubscr);
  }

  statusForm() {
    this.reportForm = this.fb.group({
      status: [''],
      vendor: [''],
    });
    const statusEvent = this.reportForm.controls.status.valueChanges.subscribe(
      (val) => {
        this.searchText = val;
        this.getCategoryList(this.currentVenderId);
      }
    );
    this.subscriptions.push(statusEvent);
    const vendorEvent = this.reportForm.controls.vendor.valueChanges.subscribe(
      (val) => {
        this.currentVenderId = val;
        if (val === '') this.currentCategoryId = '';
        this.getCategoryList(val);
      }
    );
    this.subscriptions.push(vendorEvent);
  }
}
