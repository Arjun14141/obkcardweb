import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MatIconModule } from '@angular/material/icon';
import { TranslateModule } from '@ngx-translate/core';
import { NgPagination } from './ng-pagination/ng-pagination.component';
import { PaginatorComponent } from './paginator.component';

@NgModule({
  declarations: [PaginatorComponent, NgPagination],
  imports: [CommonModule, FormsModule, MatIconModule, TranslateModule],
  exports: [PaginatorComponent, NgPagination],
})
export class PaginatorModule {}
