export interface HttpResponse {
  code: string; // "OK"
  data: any; // {user: {address: "f1", role: "admin", contact: 0, balance: 0, currencyId: null, isDeleted: false,…},…}
  message: string; // "Successfully login."
  status: number; // 200
}
