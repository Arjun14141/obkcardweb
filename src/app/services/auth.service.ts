import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { AuthHTTPService } from './auth-http.service';
import { EncryptService } from './encrypt.service';

@Injectable({ providedIn: 'root' })
export class AuthService {
  private authLocalStorageToken: string = `${environment.appVersion}-${environment.USERDATA_KEY}`;

  // public fields
  currentUser$: Observable<any>;
  currentUserSubject: BehaviorSubject<any>;

  get currentUserValue(): any {
    return this.currentUserSubject.value;
  }

  set currentUserValue(user: any) {
    this.currentUserSubject.next(user);
  }

  constructor(
    private router: Router,
    private authHttpService: AuthHTTPService,
    private encryptService: EncryptService
  ) {
    this.currentUserSubject = new BehaviorSubject<any>(undefined);
    this.currentUser$ = this.currentUserSubject.asObservable();
  }

  login(loginDetails: any): Observable<any> {
    return this.authHttpService.login(loginDetails).pipe(
      map((auth) => this.setAuthFromLocalStorage(auth.data)),
      switchMap(() => this.getUserByToken()),
      catchError((err) => {
        console.error('err', err);
        return of(undefined);
      })
    );
  }

  logout() {
    document.body.classList.add('loadingPointer');
    const auth = this.getAuthFromLocalStorage();
    this.authHttpService.logout(auth?.refresh?.token).subscribe(
      (data) => {
        if (data.status === 200) {
          localStorage.clear();
          this.router.navigate(['/home/landing']);
          document.body.classList.remove('dir_rtl');
          document.body.classList.remove('loadingPointer');
        }
      },
      (err) => {
        console.error('err', err);
        document.body.classList.remove('loadingPointer');
      }
    );
  }

  getUserByToken(): Observable<any> {
    const auth = this.getAuthFromLocalStorage();
    if (!auth || !auth.access.token) {
      return of(undefined);
    }
    return this.authHttpService.getUserByToken(auth.access.token).pipe(
      map((user) => {
        if (user) this.currentUserSubject = new BehaviorSubject<any>(user.data);
        else this.logout();
        console.log(this.currentUserValue);
        return user;
      })
    );
  }

  // private methods
  setAuthFromLocalStorage(auth: any): boolean {
    // store auth token/refreshToken/epiresIn in local storage to keep user logged in between page refreshes
    console.log(auth);
    if (auth) {
      localStorage.setItem(
        this.authLocalStorageToken,
        JSON.stringify(auth.tokens)
      );
      auth.user.password = this.encryptService.crypt(auth.user.password);
      localStorage.setItem('user', JSON.stringify(auth.user));
      return true;
    }
    return false;
  }

  getAuthFromLocalStorage() {
    try {
      const auth = localStorage.getItem(this.authLocalStorageToken);
      if (auth) return JSON.parse(auth);
    } catch (error) {
      console.error(error);
      return undefined;
    }
  }
}
