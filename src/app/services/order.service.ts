import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

const API_USERS_URL = `${environment.obkApiEndPoint}`;
@Injectable({ providedIn: 'root' })
export class OrderService {
  constructor(private http: HttpClient) {}

  orderList(
    page: number,
    limit: number,
    searchText: string,
    startDate: Date,
    endDate: Date,
    venderId: string,
    categoryId: string,
    subCategoryId: string
  ): Observable<any> {
    const url = `${API_USERS_URL}order/paginate`;
    let payload = {
      page: page,
      limit: limit,
      isCustomer: false,
      isDirectCharge: false,
    };
    if (searchText) payload['searchText'] = searchText;
    if (startDate) payload['startDate'] = startDate;
    if (endDate) payload['endDate'] = endDate;
    if (venderId) payload['venderId'] = venderId;
    if (categoryId) payload['categoryId'] = categoryId;
    if (subCategoryId) payload['subCategoryId'] = subCategoryId;
    return this.http.post<any>(url, payload);
  }

  DCOrderList(
    page: number,
    limit: number,
    searchText: string,
    startDate: string,
    endDate: string
  ): Observable<any> {
    const url = `${API_USERS_URL}order/paginate`;
    let payload = {
      page: page,
      limit: limit,
      isCustomer: false,
      isDirectCharge: true,
    };
    if (searchText) payload['searchText'] = searchText;
    if (startDate) payload['startDate'] = startDate;
    if (endDate) payload['endDate'] = endDate;
    return this.http.post<any>(url, payload);
  }

  completeOrder(id: string): Observable<any> {
    const url = `${API_USERS_URL}order/update/${id}`;
    return this.http.post<any>(url, {});
  }
}
