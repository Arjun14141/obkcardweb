import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({ providedIn: 'root' })
export class EncryptService {
  constructor() {}

  crypt = (text: string) => {
    const textToChars = (text: string) =>
      text.split('').map((c: string) => c.charCodeAt(0));
    const byteHex = (n: any) => ('0' + Number(n).toString(16)).substr(-2);
    const applySaltToChar = (code: any) =>
      textToChars(environment.USERDATA_KEY).reduce(
        (a: number, b: number) => a ^ b,
        code
      );
    return text
      .split('')
      .map(textToChars)
      .map(applySaltToChar)
      .map(byteHex)
      .join('');
  };

  decrypt = (encoded: string) => {
    const textToChars = (text: string) =>
      text.split('').map((c: string) => c.charCodeAt(0));
    const applySaltToChar = (code: any) =>
      textToChars(environment.USERDATA_KEY).reduce(
        (a: number, b: number) => a ^ b,
        code
      );
    return encoded
      .match(/.{1,2}/g)
      .map((hex: string) => parseInt(hex, 16))
      .map(applySaltToChar)
      .map((charCode: number) => String.fromCharCode(charCode))
      .join('');
  };
}
