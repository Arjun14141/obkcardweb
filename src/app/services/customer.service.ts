import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

const API_USERS_URL = `${environment.obkApiEndPoint}`;

@Injectable({ providedIn: 'root' })
export class CustomerService {
  constructor(private http: HttpClient) {}

  customerList(page: number, limit: number, searchText: string): Observable<any> {
    const url = `${API_USERS_URL}customer/paginate`;
    let payload = { page: page, limit: limit };
    if (searchText) payload['searchText'] = searchText;
    return this.http.post<any>(url, payload);
  }

  getCustomer(id: string): Observable<any> {
    const url = `${API_USERS_URL}customer/${id}`;
    return this.http.get<any>(url);
  }

  createCustomer(customerData: any): Observable<any> {
    const url = `${API_USERS_URL}customer/create`;
    return this.http.post<any>(url, customerData);
  }

  updateCustomer(customerData: any, id: string): Observable<any> {
    const url = `${API_USERS_URL}customer/update/${id}`;
    return this.http.post<any>(url, customerData);
  }

  deleteCustomer(id: string): Observable<any> {
    const url = `${API_USERS_URL}customer/delete/${id}`;
    return this.http.delete<any>(url);
  }

  unblockCustomer(id: string): Observable<any> {
    const url = `${API_USERS_URL}customer/unblock/${id}`;
    return this.http.delete<any>(url, {});
  }

  updateProfile(customerData: any): Observable<any> {
    const url = `${API_USERS_URL}admin/profile`;
    return this.http.post<any>(url, customerData);
  }

  updateBalance(id: string, amount: number): Observable<any> {
    const url = `${API_USERS_URL}transaction/update/${id}`;
    return this.http.post<any>(url, { amount: amount });
  }

  AddBalance(id: string, amount: number): Observable<any> {
    const url = `${API_USERS_URL}transaction/add/${id}`;
    return this.http.post<any>(url, { amount: amount });
  }

  transactionList(
    page: number,
    limit: number,
    adminId: string,
    customerId: string
  ): Observable<any> {
    const url = `${API_USERS_URL}transaction/report`;
    let payload = { page: page, limit: limit };
    if (adminId) payload['adminId'] = adminId;
    if (customerId) payload['customerId'] = customerId;
    return this.http.post<any>(url, payload);
  }
}
