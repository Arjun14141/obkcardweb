import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

const API_USERS_URL = `${environment.obkApiEndPoint}`;

@Injectable({ providedIn: 'root' })
export class PriceService {
  constructor(private http: HttpClient) {}

  priceList(page: number, limit: number): Observable<any> {
    const url = `${API_USERS_URL}price/paginate`;
    return this.http.post<any>(url, { page: page, limit: limit });
  }

  getPrice(id: string): Observable<any> {
    const url = `${API_USERS_URL}price/${id}`;
    return this.http.get<any>(url);
  }

  createPrice(priceData: any): Observable<any> {
    const url = `${API_USERS_URL}price/create`;
    return this.http.post<any>(url, priceData);
  }

  updatePrice(priceData: any, id: string): Observable<any> {
    const url = `${API_USERS_URL}price/update/${id}`;
    return this.http.post<any>(url, priceData);
  }

  deletePrice(id: string): Observable<any> {
    const url = `${API_USERS_URL}price/delete/${id}`;
    return this.http.delete<any>(url);
  }
}
