import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

const API_USERS_URL = `${environment.obkApiEndPoint}`;

@Injectable({ providedIn: 'root' })
export class AuthHTTPService {
  constructor(private http: HttpClient) {}

  // public methods
  login(loginDetails: any): Observable<any> {
    const url = `${API_USERS_URL}auth/login`;
    return this.http.post<any>(url, loginDetails);
  }

  refreshToken(expireToken: string): Observable<any> {
    const url = `${API_USERS_URL}auth/refresh-tokens`;
    return this.http.post<any>(url, { refreshToken: expireToken });
  }

  logout(expireToken: string): Observable<any> {
    const url = `${API_USERS_URL}auth/logout`;
    return this.http.post<any>(url, { refreshToken: expireToken });
  }

  getUserByToken(token: string): Observable<any> {
    const url = `${API_USERS_URL}admin/getprofile`;
    return this.http.get<any>(url);
  }
}
