import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthService } from './auth.service';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {
  constructor(private authService: AuthService, private router: Router) {}

  canActivate(): boolean {
    if (!!localStorage.getItem('user')) return true;
    // logged in so return true
    // not logged in so redirect to login page with the return url
    this.router.navigate(['home/landing']);
    this.authService.logout();
    return false;
  }
}
