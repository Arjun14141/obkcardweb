import {
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpRequest,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { catchError, switchMap } from 'rxjs/operators';
import { AuthService } from 'src/app/services/auth.service';
import { environment } from 'src/environments/environment';
import { AuthHTTPService } from './auth-http.service';

const API_USERS_URL = `${environment.telecomApiEndPoint}`;

@Injectable({ providedIn: 'root' })
export class TokenInterceptorService {
  private isRefreshing = false;
  authLocalStorageToken = `${environment.appVersion}-${environment.USERDATA_KEY}`;
  private refreshTokenSubject: BehaviorSubject<any> = new BehaviorSubject<any>(
    null
  );
  constructor(
    private authService: AuthService,
    private authHTTPService: AuthHTTPService
  ) {}
  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const auth = localStorage.getItem(this.authLocalStorageToken);
    if (auth) var authData = JSON.parse(auth);
    const token = `Bearer ${authData?.access?.token}`;
    return next.handle(this.addTokenHeader(req, token)).pipe(
      catchError((error) => {
        if (
          error instanceof HttpErrorResponse &&
          !req.url.includes('auth/login') &&
          error.status === 401
        )
          return this.handle401Error(req, next);
        return throwError(error);
      })
    );
  }

  private handle401Error(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<any> {
    if (!this.isRefreshing) {
      this.isRefreshing = true;
      this.refreshTokenSubject.next(null);
      const auth = this.authService.getAuthFromLocalStorage();
      const token = auth.refresh.token;
      if (token)
        return this.authHTTPService.refreshToken(token).pipe(
          switchMap((token: any) => {
            this.isRefreshing = false;
            localStorage.setItem(
              this.authLocalStorageToken,
              JSON.stringify(token)
            );
            this.refreshTokenSubject.next(token.access.token);
            return next.handle(
              this.addTokenHeader(request, `Bearer ${token.access.token}`)
            );
          }),
          catchError((err) => {
            this.isRefreshing = false;
            this.authService.logout();
            return throwError(err);
          })
        );
    } else {
      this.isRefreshing = false;
      this.authService.logout();
      return throwError('session expired, please login again');
    }
    // return this.refreshTokenSubject.pipe(
    //   filter((token) => token !== null),
    //   take(1),
    //   switchMap((token) =>
    //     next.handle(this.addTokenHeader(request, `Bearer ${token}`))
    //   ),
    // );
  }

  private addTokenHeader(request: HttpRequest<any>, token: string) {
    if (!token) return;
    if (request.url.includes('i18n')) {
      return request.clone({
        headers: request.headers.append('authorization', token),
      });
    }
    if (localStorage.getItem('type') === 'telecom') {
      return request.clone({
        headers: request.headers.append('authorization', token),
        url: `${API_USERS_URL}${request.url.split('v1')[1]}`,
      });
    }
    return request.clone({
      headers: request.headers.append('authorization', token),
    });
  }
}
