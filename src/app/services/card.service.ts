import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

const API_USERS_URL = `${environment.obkApiEndPoint}`;

@Injectable({ providedIn: 'root' })
export class CardService {
  constructor(private http: HttpClient) {}

  cardList(
    page: number,
    limit: number,
    adminId: string,
    searchText: string,
    venderId: string,
    categoryId: string,
    subCategoryId: string
  ): Observable<any> {
    const url = `${API_USERS_URL}card/paginate`;
    let payload = {
      page: page,
      limit: limit,
      adminId: adminId,
    };
    if (searchText) payload['searchText'] = searchText;
    if (venderId) payload['venderId'] = venderId;
    if (categoryId) payload['categoryId'] = categoryId;
    if (subCategoryId) payload['subCategoryId'] = subCategoryId;
    return this.http.post<any>(url, payload);
  }

  getCard(id: string): Observable<any> {
    const url = `${API_USERS_URL}card/${id}`;
    return this.http.get<any>(url);
  }

  createCard(cardData: any): Observable<any> {
    const url = `${API_USERS_URL}card/create`;
    return this.http.post<any>(url, cardData);
  }

  importCardList(cardData: any[]): Observable<any> {
    const url = `${API_USERS_URL}card/excel`;
    return this.http.post<any>(url, cardData);
  }

  updateCard(cardData: any, id: string): Observable<any> {
    const url = `${API_USERS_URL}card/update/${id}`;
    return this.http.post<any>(url, cardData);
  }

  deleteCard(id: string): Observable<any> {
    const url = `${API_USERS_URL}card/delete/${id}`;
    return this.http.delete<any>(url);
  }

  venderList(): Observable<any> {
    const url = `${API_USERS_URL}vender/paginate`;
    return this.http.post<any>(url, {});
  }

  categoryList(venderId: string): Observable<any> {
    const url = `${API_USERS_URL}category/paginate`;
    let payload = {};
    if (venderId) payload['venderId'] = venderId;
    return this.http.post<any>(url, payload);
  }

  subCategoryList(categoryId: string): Observable<any> {
    const url = `${API_USERS_URL}subcategory/paginate`;
    return this.http.post<any>(url, { categoryId: categoryId });
  }

  reportList(
    adminId: string,
    searchText: string,
    venderId: string,
    categoryId: string
  ): Observable<any> {
    const url = `${API_USERS_URL}card/report`;
    let payload = { adminId: adminId };
    if (searchText) payload['searchText'] = searchText;
    if (venderId) payload['venderId'] = venderId;
    if (categoryId) payload['categoryId'] = categoryId;
    return this.http.post<any>(url, payload);
  }
}
