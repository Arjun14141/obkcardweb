export const environment = {
  production: true,
  // obkApiEndPoint: 'https://18.116.225.114:3000/v1/', //dev
  // obkApiEndPoint: 'http://172.35.0.50:3000/v1/', //new
  obkApiEndPoint: 'https://ryancardsapi.com/v1/', //new dev
  telecomApiEndPoint: 'https://ryancardsapi.com:444/v1', //new dev
  // obkApiEndPoint: 'http://localhost:3000/v1/', //local
  appVersion: 'v726demo1',
  USERDATA_KEY: 'authf649fc9a5f55',
  isMockEnabled: true,
};
