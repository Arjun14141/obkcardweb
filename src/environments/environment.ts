// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  // obkApiEndPoint: 'https://18.116.225.114:3000/v1/', //dev
  obkApiEndPoint: 'https://ryancardsapi.com/v1/', //new dev
  telecomApiEndPoint: 'https://ryancardsapi.com:444/v1', //new dev //telecom url won't have last '/'
  // obkApiEndPoint: 'http://localhost:3000/v1/', //local
  appVersion: 'v726demo1',
  USERDATA_KEY: 'authf649fc9a5f55',
  isMockEnabled: true,
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
